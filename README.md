# GDash #

**GDash** is a Boulder Dash clone. The main goal of the project is to implement a clone which is as close to the original as possible.

The game runs under Windows, Linux and other Unix clones, and also on Mac. For compiling, you need the GTK+ dev files, SDL2, SDL2_Image and SDL2_Mixer (with dev files). Read more about GDash in the [Boulder Dash Forum](http://www.boulder-dash.nl/forum/viewtopic.php?t=121).

GDash has a cave editor, supports sound, joystick and keyboard controls. It can use GTK+, SDL2 and OpenGL for drawing. The OpenGL engine can use shaders, which provide fullscreen graphical effects like TV screen emulation.

![GDash](https://bitbucket.org/repo/ejzABR/images/4034423466-gdashgame.png)

## License ##

The game is licensed under MIT license terms. See the COPYING file in the source tree.

## Ports ##

GDash has an unofficial [Pandora port](http://repo.openpandora.org/?page=detail&app=gdash).

For a Mac port, see [Mac port](https://www.macports.org/).
